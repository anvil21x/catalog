package com.tricomtek.ar.catalog;

import android.content.Context;
import android.util.Log;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.ReadContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * assets/data.json 의 JSON 파일을 읽어 데이터형태로 제공한다.
 */
public class Data {

    private static JSONArray dataArray;

    public static void Load(Context context) {

        StringBuffer sb = new StringBuffer();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(context.getAssets().open("data.json")));
            // read every line of the file into the line-variable, on line at the time
            String line;
            do {
                line = br.readLine();
                sb.append(line + "\n");

                // if (line != null) System.out.println(line);

                // do something with the line
            } while (line != null);
        } catch (IOException e) {
            e.printStackTrace();
        }


        // JSON 데이터 로드
        try {
            Data.SetData(new JSONArray(sb.toString()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void SetData(JSONArray dataArray) {
        Data.dataArray = dataArray;
    }

    /**
     * 비지니스 부문을 리스트로 반환한다.
     * @return
     */
    public static List<String> GetBizList() {

        List<String> list = new ArrayList<String>();

        for (int i = 0; i < dataArray.length(); i++) {
            try {
                JSONObject biz = (JSONObject) dataArray.get(i);
                list.add((String) biz.get("biz"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return list;
    }

    /**
     * 각 비지니스의 파트 부문을 리스트로 반환한다.
     * @param bizName
     * @return
     */
    public static List<String> GetPartListByBiz(String bizName) {

        List<String> list = new ArrayList<String>();

        int index = GetBizList().indexOf(bizName);

        try {
            JSONObject biz = (JSONObject) dataArray.get(index);
            JSONArray parts = (JSONArray) biz.get("part");

            for (int i = 0; i < parts.length(); i++) {
                try {
                    JSONObject part = (JSONObject) parts.get(i);
                    //                    Log.v("TAG", part.toString(2));
                    list.add((String) part.get("name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return list;

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * 제품이름과 URL 을 Map 형태로 반환한다.
     * @param bizName
     * @param partName
     * @return
     */
    public static Map<String, String> GetProductMapByBizAndPart(String bizName, String partName) {

        System.out.println(bizName + " " + partName);
        Map<String, String> map = new LinkedHashMap<String, String>();

        int bizIndex = GetBizList().indexOf(bizName);
        int partIndex = GetPartListByBiz(bizName).indexOf(partName);

        try {
            JSONObject biz = (JSONObject) dataArray.get(bizIndex);
            JSONArray parts = (JSONArray) biz.get("part");
            JSONObject part = (JSONObject) parts.get(partIndex);
            JSONArray products = (JSONArray) part.get("products");

            for (int i = 0; i < products.length(); i++) {
                try {
                    JSONObject item = (JSONObject) products.get(i);

                    map.put(item.get("name").toString(),
                            item.get("url").toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return map;
    }


    public static String GetJSONDataToString() throws JSONException {
        return dataArray.toString(2);
    }

}

/**
 * 다른 JSON 파서 테스트
 */
//        ReadContext parser = null;
//        try {
//            parser = JsonPath.parse(Data.GetJSONDataToString());
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        List<String> bizList
//                = parser.read("$[*][?(@.biz == '" + bizName + "')].part.[?(@.name == '" + partName + "')].products", String.class);
//        = parser.read("$[*][?(@.biz == '유무선 테스트')].part.[?(@.name == '모바일 통신 부문')].products[?(@.name == 'Packetstorm')]", List.class);
//        = parser.read("$[*][?(@.biz == '유무선 테스트')].part.[?(@.name == '모바일 통신 부문')].products[?(@.name)]", List.class);
//        System.out.println("--- " + bizList);
//        List<String> bizList = parser.read("$[*].[?(@.biz == \""+ bizName+  "\")]", List.class);
//        for (int i = 0; i < bizList.size(); i++) {
//            Log.v("tag", bizList.get(i));
//        }
