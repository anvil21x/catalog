package com.tricomtek.ar.catalog;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;


public class BusinessPartActivity extends ActionBarActivity
        implements BusinessPartListFragment.Callbacks {

    /**
     * 타블렛 모드 플래그
     */
    public static boolean TwoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * {@link R.layout.activity_item_list} 은 스크린 사이즈에 따라 두 가지 레이아웃으로 자동으로 선택된다.
         * 앱이 구동될때 refs.xml 의 내용에 따라 대형화면일 경우 {@link R.layout.main_twopane_table} 을
         * 선택하고 작은 화면일 경우 {@link R.layout.main_twopane_table} 을 선택한다.
         *
         */

        // 폰, 타블렛 자동 선택
        setContentView(R.layout.activity_item_list);

        // 강제 타블렛 모드
        //setContentView(R.layout.main_twopane_table);

        // 강제 폰 모드
        // setContentView(R.layout.main_onepane_phone);

        if (findViewById(R.id.item_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-large and
            // res/values-sw600dp). If this view is present, then the
            // activity should be in two-pane mode.
            TwoPane = true;

            // In two-pane mode, list items should be given the
            // 'activated' state when touched.
            ((BusinessPartListFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.item_list))
                    .setActivateOnItemClick(true);

            System.out.println(TwoPane ? "TabletMode" : "PhoneMode");

        }

    }

    /**
     * 메인에서 비지니스 부문을 선택했을때 이벤트
     */
    @Override
    public void onItemSelected(String name) {

        /**
         * 타블렛 UI 인 경우 바로 Fragment 를 가져와서 디스플레이한다
         * 폰의 경우 새로운 Activity 하나 더 띄우고 그 내부에 Fragment 를 디스플레이한다.
         */

        Bundle bundle = new Bundle();
        bundle.putString(ItemDetailListFragment.ARG_BIZ, name);
        bundle.putInt(ItemDetailListFragment.ARG_DEPTH, 1);

        if (TwoPane) {

            android.support.v4.app.FragmentTransaction transaction
                    = getSupportFragmentManager().beginTransaction();
            Fragment existFragment
                    = getSupportFragmentManager().findFragmentById(R.id.item_detail_container);

            if (existFragment != null) {
                transaction.remove(existFragment);
                transaction.commit();
            }

            ItemDetailListFragment fragment = new ItemDetailListFragment();
            fragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.item_detail_container, fragment)
                    .commit();

        } else {

            Intent detailIntent = new Intent(this, ItemDetailActivity.class);
            detailIntent.putExtras(bundle);
            startActivity(detailIntent);

        }
    }

}
