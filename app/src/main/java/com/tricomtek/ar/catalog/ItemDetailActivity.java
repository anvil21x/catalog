package com.tricomtek.ar.catalog;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;


/**
 * 폰 모드일 경우 Fragment 를 열기위해 겉에 씌워주는 Activity
 */
public class ItemDetailActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sub_activity_item_detail);

        // 액션바 이전이동 버튼 보이기
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null) {

            Bundle arguments = getIntent().getExtras();
            String bizName = arguments.getString(ItemDetailListFragment.ARG_BIZ);
            String partName = arguments.getString(ItemDetailListFragment.ARG_PART);
            String productName = arguments.getString(ItemDetailListFragment.ARG_PRODUCT);
            int depth = arguments.getInt(ItemDetailListFragment.ARG_DEPTH);

            // 액션바 타이틀 이름 설정, 프레그먼트 설정
            String title = null;
            Fragment fragment = null;

            switch (depth) {
                case 1:
                    title = bizName;
                    fragment = new ItemDetailListFragment();
                    break;
                case 2:
                    title = partName;
                    fragment = new ItemDetailListFragment();
                    break;
                case 3:
                    title = productName;
                    fragment = new ItemDetailWebViewFragment();
                    break;
            }
            getSupportActionBar().setTitle(title);
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.item_detail_container, fragment)
                    .commit();

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
