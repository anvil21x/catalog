package com.tricomtek.ar.catalog;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

/**
 * 웹뷰 Fragment
 */
public class ItemDetailWebViewFragment extends Fragment {

    public static final String ARG_URL = "url";

    private String currentURL;
    private WebView wv;
    private ProgressBar progressBar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Fragment 아규먼트에서 URL 을 가져온다
        currentURL = getArguments().getString(ARG_URL);

        Log.d("SwA", "WVF onCreateView");
        View v = inflater.inflate(R.layout.sub_fragment_webview, container, false);
        if (currentURL != null) {
            Log.d("SwA", "Current URL  1[" + currentURL + "]");

            wv = (WebView) v.findViewById(R.id.webView2);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar);

            wv.getSettings().setJavaScriptEnabled(true);
            wv.setWebViewClient(new SwAWebClient());


            wv.setWebChromeClient(new WebChromeClient() {

                @Override
                public void onProgressChanged(WebView view, int newProgress) {
                    //super.onProgressChanged(view, newProgress);
                    progressBar.setProgress(newProgress);


                    progressBar.setVisibility((newProgress == 100)
                            ? View.INVISIBLE
                            : View.VISIBLE);


                }
            });


            wv.loadUrl(currentURL);
        }
        return v;
    }


    private class SwAWebClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return false;
        }

    }

}
