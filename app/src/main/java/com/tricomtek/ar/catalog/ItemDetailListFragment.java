package com.tricomtek.ar.catalog;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 메인메뉴에서 선택된 항목의 하위 리스트 아이템들이 표시되는 부문
 */
public class ItemDetailListFragment extends ListFragment {

    public static final String ARG_BIZ = "biz";
    public static final String ARG_PART = "part";
    public static final String ARG_PRODUCT = "product";
    public static final String ARG_DEPTH = "depth";

    private Bundle bundle;
    private int depth;
    private String mBizName;
    private String mPartName;
    private String mProductName;

    private List<String> list;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // 원본을 유지하기 위해 Bundle을 복사함
        bundle = (Bundle) getArguments().clone();
        depth = bundle.getInt(ARG_DEPTH);
        mBizName = bundle.getString(ARG_BIZ);
        mPartName = bundle.getString(ARG_PART);
        mProductName = bundle.getString(ARG_PRODUCT);

        Log.v("TAG", "Create " + mBizName + " " + mPartName + " " + mProductName + " " + depth);

        switch (depth) {
            case 1:
                list = Data.GetPartListByBiz(mBizName);
                break;

            case 2:
                Map<String, String> productMap = Data.GetProductMapByBizAndPart(mBizName, mPartName);
                list = Arrays.asList(productMap.keySet().toArray(new String[productMap.size()]));
                break;
        }

        setListAdapter(new ArrayAdapter<String>(
                getActivity(),
                android.R.layout.simple_list_item_activated_1,
                android.R.id.text1,
                list));

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    /**
     * 리스트의 아이템들을 선택했을 경우 이벤트 처리
     *
     * @param l
     * @param v
     * @param position
     * @param id
     */
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        bundle.putInt(ItemDetailListFragment.ARG_DEPTH, depth + 1);

        switch (depth) {
            case 1:
                mPartName = list.get(position);
                bundle.putString(ItemDetailListFragment.ARG_PART, mPartName);
                break;

            case 2:
                mProductName = list.get(position);
                Map<String, String> productMap = Data.GetProductMapByBizAndPart(mBizName, mPartName);
                String url = productMap.get(list.get(position));
                bundle.putString(ItemDetailListFragment.ARG_PRODUCT, mProductName);
                bundle.putString(ItemDetailWebViewFragment.ARG_URL, url);
                break;
        }

        System.out.println("Selected " + mBizName + " " + mPartName + " " + depth);

        // 타블렛 모드
        if (BusinessPartActivity.TwoPane == true) {

            // 액션바 타이틀 이름 설정, 프레그먼트 설정
            FragmentTransaction transaction
                    = getActivity()
                    .getSupportFragmentManager().beginTransaction();
            Fragment existFragment
                    = getActivity()
                    .getSupportFragmentManager()
                    .findFragmentById(R.id.item_detail_container);

            if (existFragment != null) {
                transaction.remove(existFragment);
                transaction.commit();
            }

            // Fragment 파라미터 설정
            Fragment fragment = null;
            fragment = (depth == 1) ? new ItemDetailListFragment() : fragment;
            fragment = (depth == 2) ? new ItemDetailWebViewFragment() : fragment;
            System.out.println("Depth : " + depth);
            fragment.setArguments(bundle);

            getActivity().getSupportFragmentManager().beginTransaction()
                    .add(R.id.item_detail_container, fragment)
                    .commit();

        }

        // 폰 모드
        else {

            Intent detailIntent = new Intent(getActivity(), ItemDetailActivity.class);
            detailIntent.putExtras(bundle);
            startActivity(detailIntent);

        }
    }

    @Override
    public void onResume() {
        super.onResume();

        mBizName = bundle.getString(ARG_BIZ);
        mPartName = bundle.getString(ARG_PART);
        mProductName = bundle.getString(ARG_PRODUCT);

        depth = getArguments().getInt(ARG_DEPTH);

        Log.v("TAG", "resume " + mBizName + " " + mPartName + " " + mProductName + " " + depth);
    }
}
